export interface Product{
    id:number;
    name:string;
    description:string;
    price:number;
    pictureUrl:string;
}

export interface Category {
    
    id: String;
    name: String;
}
